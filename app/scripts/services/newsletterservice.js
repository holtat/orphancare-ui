'use strict';

angular.module('orphanCareApp')
  .service('Newsletterservice', function Newsletterservice($http) {
    // AngularJS will instantiate a singleton by calling "new" on this function
      return { 
          getNewsletters: function() {
            return [
							{
								"id" : "0314",
								"url" : "views/Newsletters/April2014.html",
								"title" : "Safe Families Org",
								"date" : "April 2014"

							},
							{
								"id" : "0214",
								"url" : "views/Newsletters/February2014.html",
								"title" : "Prayers Answered in Honduras",
								"date" : "February 2014"
							}
						];
          }
    }
  });
