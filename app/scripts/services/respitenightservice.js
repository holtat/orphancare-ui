'use strict';

angular.module('orphanCareApp')
.service('Respitenightservice', function Respitenightservice($http, toaster, $rootScope) {
	return {
		submitSignup : function(data, success) {
			var json = angular.toJson(data);
			$http({
				url: 'http://nworphancare.org/api/RespiteNightRegistration',
				method: "POST",
				data: json,
				headers: {'Content-Type': 'application/json'}
			}).success(function (data, status, headers, config) {
				toaster.pop('success', "Great!", "You're signed up.");
				success();
			}).error(function (data, status, headers, config) {
				toaster.pop('error', "Sorry", status +  ": " + data, 8000);
				console.log(status);
			});
		},
		getSignups : function(complete) {
			$http({
				url: 'http://nworphancare.org/api/RespiteNightRegistration',
				method: "GET",
			}).success(function (data, status, headers, config) {
				toaster.pop('success', "Success", "Here's the signups");
				complete(data);
			}).error(function (data, status, headers, config) {
				toaster.pop('error', "Whoops", data.Message);
				complete(data);
			});	
		}
	}
});
