'use strict';

angular.module('orphanCareApp')
.service('UpcomingEventservice', function UpcomingEventservice($http) {

    var filterByAfterToday = function(element) {
        var today = new Date();
        return today.getTime() < element.date.getTime();
    };

    var upcomingEvents = [
        {
            name: "MOMs Group Family Picnic",
            date: new Date(2014, 7, 20),
            time: "5:30 - Dusk",
            location: "Devlin Park, McCandless",
            notes: "Contact orphancare@northway.org for more information"
        },
        {
            name: "Pre-Adoption Group",
            date: new Date(2015, 3, 12),
            time: "7:00 - 8:00",
            location: "Founders Room C, NW Wexford"
        },
        {
            name: "MOMs Support Group",
            date: new Date(2015, 3, 15),
            time: "6:30 – 8:30",
            location: "Founders Room C, NW Wexford"
        },
        {
            name: "OrphanCare Team Meeting",
            date: new Date(2015, 3, 19),
            time: "8:45 – 10:45 a.m.",
            location: "Trinity Room, NW Wexford"
        },
        {
            name: "Respite Night Event",
            date: new Date(2015, 4, 1),
            time: "6:30 – 8:30",
            notes: "Registration is required"
        },
        {
            name: "Embrace Life Ministries Meeting",
            date: new Date(2015, 4, 7),
            time: "7:00 - 9:00",
            location: "Multi-site Conference Room, NW Wexford"
        },
        {
            date: new Date(2015, 4, 13),
            name: 'MOMs Support Group',
            time: '6:30 - 8:30 pm',
            location: 'Founders Room C, NW Wexford'
        },
        {
            date: new Date(2015, 4, 17),
            name: 'Keeping Kids Safe Online',
            time: '6:30 pm',
            location: 'NW Wexford',
            notes: '45 minute presentation for parents by the FBI. For more info visit northway.org/justice'
        },
        {
            date: new Date(2015, 4, 17),
            name: 'Pre-Adoption Group ',
            time: '7:00 - 8:30 pm',
            location: 'Founders Room C, NW Wexford',
        },
        {
            date: new Date(2015, 4, 28),
            name: 'Focused Prayer for Foster Care',
            time: '7:00 – 8:00 pm',
            location: 'Trinity Room, NW Wexford',
            notes: 'Screening of Remember My Story(ReMoved 2)'
        },
        {
            date: new Date(2015, 5, 4),
            name: 'Embrace Life Ministries Meeting ',
            time: '7:00 - 9:00 pm',
            location: 'Multi-site Conference Room, NW Wexford'
        },
        {
            date: new Date(2015, 5, 17),
            name: 'MOMs Group Family Picnic & Adoption Celebration',
            time: '5:30 to dusk',
            location: 'Devlin Park, McCandless',
            notes: 'orphancare@northway.org for more information'
        },
        {
            date: new Date(2015, 6, 9),
            name: 'Embrace Life Ministries Meeting ',
            time: '7:00 - 9:00 pm ',
            location: 'Multi-site Conference Room, NW Wexford'
        },
        {
            date: new Date(2015, 6, 12),
            name: 'Pre-Adoption Group ',
            time: '7:00 - 8:30 pm ',
            location: 'Founders Room C, NW Wexford'
        },
        {
            date: new Date(2015, 6, 15),
            name: 'Adoptive & Foster MOMs',
            time: '6:30 - 8:30 pm',
            location: 'Founders Room C, NW Wexford'
        },
        {
        date: new Date(2015, 6, 19),
        name: "Justice Team Meeting",
        time: "4:00 – 5:30 p.m.",
        location: "Founders A, NW Wexford",
        notes: "Training: Combatting Demand & the Misconception about Prostitution"
        },
        {
        date: new Date(2015, 7, 9),
        name: "“You’re a Star”",
        time: "2:00 – 4:00 p.m.",
        location: "Atrium, NW Wexford",
        notes: "embracelife@northway.org for more information. Formal Dance for our friends with special needs"
        },
        {
        date: new Date(2015, 7, 11),
        name: "Justice Team Volunteer Day",
        time: "10 a.m. to 1 p.m.",
        location: "RePurposed Store",
        notes: "justice@northway.org for more information"
        },
        {
        date: new Date(2015, 7, 12),
        name: "Pre-Adoption Support Group",
        time: "6:30 – 8:00 p.m.",
        location: "Conference Room, NW Wexford",
        notes: "“No Drama Discipline” Study"
        },
        {
        date: new Date(2015, 7, 16),
        name: "OrphanCare Team Meeting",
        time: "8:45 -10:45 a.m.",
        location: "Trinity Room, NW Wexford"
        },
        {
        date: new Date(2015, 7, 16),
        name: "Justice Team Meeting",
        time: "4:00 – 5:30 p.m.",
        location: "Founders A, NW Wexford",
        notes: "Guest Speaker:  Danielle Snyder; Free the Girls - El Slavador"
        },
        {
        date: new Date(2015, 7, 19),
        name: "MOMs Family Picnic & Adoption Celebration",
        time: "5:30 to dusk",
        location: "Devlin Park, McCandless",
        notes: "orphancare@northway.org for more information"
        }
        ];

    return {
        getUpcomingEvents: GetUpcomingEvents
    };

    /////////////////////

    function GetUpcomingEvents() {
        return upcomingEvents.filter(filterByAfterToday);
    }
});
