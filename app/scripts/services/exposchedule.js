'use strict';

angular.module('orphanCareApp')
.service('Exposcheduleservice', function Exposchedule() {

	return {
		getExpoSchedule: getExpoSchedule,
		getExpoSpeakers: getExpoSpeakers,
		getFaqs: getFaqs
	};


	/*****/

	function getFaqs() {
		return [
			{
				question: 'What is the location for the OrphanCare Expo?',
				answer: 'The 2014 OrphanCare Expo will be held at North Way Christian Community Church located at 12121 Perry Highway, Wexford PA  15090.'
			},
			{
				question: 'What is the cost to attend?',
				answer: 'The OrphanCare Expo is completely free!\n\nWe are passionate about providing a way for people to become educated and inspired in the areas of mentoring, missions, human trafficking, fostering and adoption. We are excited to be able to provide admission, materials, breakfast and lunch at no cost to you.  OrphanCare, Bringing Hope to the Fatherless, is a ministry of North Way Christian Community so much of our support comes from the church, and some of our support comes from families who share our passion about caring for orphans.  If you have this same passion and want to help support our ministry financially you will have an opportunity to make a financial contribution at the OrphanCare Expo on November 15th– but there is no pressure to do so.'
			},
			{
				question: 'What if I register and then need to cancel?',
				answer: 'Please read this carefully. Because we are providing this conference for free, we want to be good stewards of the resources that God has provided us.  If you are unable to attend the conference after you have already registered – please let us know immediately so that we will not incur unnecessary expense.  Your cooperation with this policy will allow us to continue to offer quality events free of charge.  To cancel your registration – simply email orphancare@northway.org.'
			},
			{
				question: 'Do you provide food and drinks for the day?',
				answer: 'We provide a very nice continental breakfast that is served during the registration time.  We will have coffee and juice, fruit, and pastries.  We also will be providing a boxed lunch consisting of a sandwich, chips, fruit and bottled water. We will provide a vegetarian option if you request it at registration.  We understand that some people have food allergies or sensitivities that need to be accommodated.  However, given the number of people we are expecting for the conference it will be impossible for us to accommodate special requests.  Therefore, if you have food allergies or sensitivities and the above menu is not acceptable for you, please make plans to bring your own food.  Thanks for your help and understanding.  There is a Sheetz and a Panera directly across the street from our building so there are also those options if you wish.'
			},
			{
				question: 'Is childcare available for the OrphanCare Expo?',
				answer: 'Limited childcare is available on a first come basis so you will want to register early.  Once registration is filled you can be added to a waiting list as we often have cancellations and can offer a spot to those waiting.  Because it is a long day, we will ask you to please pack a PEANUT FREE lunch for your child.  We must be very strict about this peanut free policy in order to protect children with severe allergies.  We will provide snacks and juices throughout the day.  If you need to cancel, please email orphancare@northway.org because we will have a waiting list with someone who would be grateful for your spot.  Childcare will be offered from 8:00 a.m. until 3:00 p.m. Any childcare questions should be addressed to  orphancare@northway.org.'
			},
			{
				question: 'Do you offer foster/adoptive parent training credits?',
				answer: 'Most foster and adoptive parents can count attendance at the OrphanCare Expo toward their required continuing education training. At the end of the conference, we will provide certificates of attendance as documentation for your agency or CPS. You can earn up to 5 hours of training credits at the Expo.  If you have questions about whether your agency will give credit, please be sure to check with your agency in advance.  If you leave the conference without obtaining your certificate, please e-mail orphancare@northway.org and we will be happy to send it to you.'
			}
		];
	}
	
	function getExpoSpeakers() {
		return [
			{
				styles: {
					'background': 'url("/images/ExpoMandyLitzke.jpg") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Mandy Litzke',
				bio: 'Born into a tragic, heart-wrenching environment of alcoholism, drug addiction and abuse, God’s first miracle was sparing Mandy’s life.  Come hear how God then proceeded to put His healing grace into action to transform what could have become bitterness into blessings.  Mandy’s adoption by her forever family led to her adoption into the family of God.  Called to an unimaginable journey of faith and obedience to live out loud James 1:27, this Mom of 16 (2 biological and 14 adopted) will share her story – a masterpiece of His design….All For His Glory.'
			},
			{
				styles: {
					'background': 'url("/images/ExpoJaneAbbate.jpg") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Jane Abbate',
				bio: 'Jane is the founder of Messy Miracles, a ministry which helps women and men who struggle with guilt, shame and regret. Her work as a leadership coach and extensive experience guiding people in behavior change, make Jane the ideal person to help others face their “messy” lives and move forward with hope. She lives the healing process she teaches: Face the past, mourn the losses, turn, and draw closer to God. Jane is the author of Where Do Broken Hearts Go? Healing and Hope After Abortion and The Spiritual Coach’s Toolkit. Jane lives in Pittsburgh, PA with her husband, Bill, and is an active member of Allison Park Church.'
			},
			{
				styles: {
					'background': 'url("/images/ExpoAmyBrooks.jpg") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Amy Brooks',
				bio: 'Amy Brooks is a joyful, exuberant, and faithful Christian whose vision is to glorify Jesus Christ by testifying to the unbeliever of His saving grace and by bringing encouragement to those who already know Him. Her writing honors her adoptive family and their unconditional love for her.'
			},
			{
				styles: {
					'background': 'url("/images/ExpoErinBrothers.jpg") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Erin Brothers',
				bio: 'Erin is an adoptive mom of an active pre-schooler and infant.  Having learned much about the Lord’s faithfulness and heart in the 4-year wait to bring their daughter home through the foster-care system and His sense of humor in their 21-hour non-wait to bring home their son, she now thoroughly enjoys finger painting, mud puddle splashing, and baby smiles.  During rare combined nap times she creates custom quilts and volunteers with the North Way OrphanCare Team.'
			},
			{
				styles: {
					'background': 'url("/images/ExpoEricaChevalier.jpg") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Erica Chevalier',
				bio: 'Erica is the Director of the Justice Team at North Way Christian Community, which advocates on behalf of the vulnerable & oppressed by engaging the church through education and outreach [www.northway.org/justice]. Additionally, she serves on the board of Mission to El Salvador [www.missiontoelsalvador.com] and also serves on the Western PA Human Trafficking Coalition [www.endhumantrafficking.org] which develops and maintains a network of individuals and agencies to educate the community about human trafficking and to provide emergent support to victims and survivors in Western PA. She is very passionate about bringing awareness to the issue of human trafficking, and she has a desire to see people around the world freed from injustice. Erica, her husband Kent, and her three beautiful children live in the Pittsburgh area.'
			},
			{
				styles: {
					'background': 'url("/images/PhilExpo2014.jpg") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Phil Gazely',
				bio: 'Phil is the Coordinator for the Kern Coalition Against Human Trafficking and the Social Justice Ministries Coordinator for Youth With a Mission City Initiatives. In addition to a background in refugee resettlement Phil was one of the founders of both the Colorado Network to End Human Trafficking and the Kern Coalition Against Human Trafficking.  He formerly trained law enforcement with the Colorado Regional Community Policing Institute. He currently provides technical assistance to anti trafficking programs in Europe, Central America and North America and has recently returned from Brazil. He trains victim service providers, health care providers and child protection staff and does prevention work with teens in foster care and at risk families. Phil is also an ordained minister, Church elder and therefore also engages with faith-based communities on anti trafficking, refugee cultural mentoring and program development in immigrant communities. He is married to Caren and they have 2 grown children. They live in Tehachapi California.'
			},
			{
				styles: {
					'background': 'url("/images/ExpoMatthewGeppert.jpg") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Matthew Geppert',
				bio: 'Matthew is the President of the South East Asia Prayer Center, which is a global community of prayer that operates in 25 nations through healthcare, education, parenting, and micro-economic development.  Matthew first joined SEAPC as a regional representative in 2003 after graduating from the University of Montana’s Environmental Studies program.  His years at SEAPC have provided him with the opportunity to serve and participate in nation-changing Christ-centered projects.'
			},
			{
				styles: {
					'background': 'url("/images/ExpoRonGoodman.jpg") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Ron Goodman',
				bio: 'Ron Goodman joined Orphan Helpers in July of 2011 as the Director of Partnerships.  Ron brings a strong ministry and education background that has prepared him well for this role, having been in ministry since 1970.  He founded and led Campus Christians, a campus ministry at the University of Kansas.  He just completed a nine-year ministry as Pastor of Discipleship at Stutsmanville Chapel in Harbor Springs, MI.  This congregation has been a strong supporter of Orphan Helpers since 2005.'
			},
			{
				styles: {
					'background': 'url("/images/ExpoKristinHimmler.jpg") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Kristin Himmler',
				bio: 'Kristin Himmler is a wife to Chad and mom to four kiddos brought to her by birth and adoption.  She lives in the East Liberty neighborhood of Pittsburgh and attends North Way\'s East End Campus.  She is active on the OrphanCare team at North Way and also serves as a one-to-one mentor through the LAMP program.  In 2008 God placed adoption on the hearts of both Kristin and Chad.  In 2011 they brought home their 9 month old baby boy from Ethiopia.  Kristin loves to talk about adoption and connect with other adoptive moms over coffee or at a Created for Care retreat designed especially to meet the needs of tired adoptive and foster moms.'
			},
			{
				styles: {
					'background': 'url("/images/ExpoShannonLibengood.jpg") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Shannon Libengood',
				bio: 'Shannon is the Director of North Way Christian Community’s local and global mission ministry.  She grew up at North Way, served 8 years on the mission field in Asia, and has been on staff since 2005 when she returned from the mission field.  Shannon and her husband live in Blairsville, PA and have 3 children.'
			},
			{
				styles: {
					'background': 'url("/images/ExpoMegMcKivigan.jpg") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Meg McKivigan',
				bio: 'Meg McKivigan M.Ed. is a mom to three children via domestic infant adoption. Eli is 3 and twins Ezra and Naomi are 1. The McKivigans have fully open adoptions with both birth families, and enjoy educating and connecting with other families on open adoption, transracial adoption, and issues related to parenting adopted children. Meg works very part time as a developmental specialist for infants and toddlers while her children are small, and enjoys being a mostly-stay-at-home-mom during this season of life. Meg blogs at www.godwillfillthisnest.com about family, faith, and adoption.'
			},
			{
				styles: {
					'background': 'url("/images/ExpoMeghanNagle.jpg") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Meghan Nagle',
				bio: 'Meghan is a passionate and skillful social worker who has established herself in her work with families and children from hard places.  She completed her field placement with Three Rivers Adoption Council and was subsequently employed as a Post Adoption Family Therapist in 2012.  She holds a Bachelor of Arts Degree in Social Work from Gannon University where she was awarded NASW-PA Northwest Division BSW Student of the Year and her Master of Social Work Degree from the University of Pittsburgh.  Ms. Nagle has conducted and presented research regarding youth with disabilities in the child welfare system at both the state and local level. This research is currently awaiting publication.  As a post-permanency therapist, Ms. Nagle has presented on the local, state and international level.'
			},
			{
				styles: {
					'background': 'url("/images/ExpoLindaPrevost.jpg") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Linda Prevost',
				bio: 'Linda has been a social worker with Bethany Christian Services for 3 years.  She is celebrating 15 years of marriage, and has two children.  Linda’s passion is to work with families who are dedicated to their children, regardless of their limitations.  She strives to advocate for the children and families who are often misunderstood in the schools and community.  She has found in her 20 years of experience working in this field that families need a voice, they need to connect with others families who are like-minded, and they need to be able to be open and honest and not fear being judged.  She feels blessed to work at Bethany Christian Services which allows her personal faith in God and her professional experiences to bless others.'
			},
			{
				styles: {
					'background': 'url("/images/ExpoSethReichert.jpg") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Seth Reichert',
				bio: 'Seth\'s calling to ministry began while he was working at Pine Valley Bible Camp as an eighth grader. After graduating from high school, Seth received a football scholarship to Liberty University where God continued to shape and equip him.  After his freshman year of college, he married his wife Nikki. The two served in two urban ministries and created a summer day camp program. God continued to shape Seth and Nikki\'s calling to work in the inner city and led them back to their hometown in Pennsylvania after graduation. Seth began working as an Urban Missionary at Urban Impact in 2006 and currently serves as the organization\'s Director of Athletics.  He lives on the North Side with his wife Nikki and their six children.'
			},
			{
				styles: {
					'background': 'url("/images/ExpoJoelRepic.jpg") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Joel Repic',
				bio: 'Joel is the founder of Aliquippa Impact, a youth development organization providing tangible hope for a purposeful future for young people facing at-risk environments. He is also co-pastor of Crestmont Alliance Church in Aliquippa, Pennsylvania. He and his family live in the Plan 12 neighborhood of Aliquippa and are honored to play a part in their community’s redemption and renewal.'
			},
			{
				styles: {
					'background': 'url("/images/ExpoKellyRyan-Schmidt.png") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Kelly Ryan-Schmidt',
				bio: 'Kelly is a Licensed Clinical Social Worker who is currently employed as the Family Connections Post-Permanency Supervisor by Three Rivers Adoption Council in Pittsburgh, PA.   Kelly specializes in post-permanency therapy for children and families. She also sees outpatient and child preparation clients.   She has been working in the fields of Adoption and Mental Health since 2001.  She is also an adoptive parent of four children from the foster care system.'
			},
			{
				styles: {
					'background': 'url("/images/ExpoHollySeipp.jpg") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Holly Seipp',
				bio: 'Holly was adopted from Korea at age 2 ½ and grew up in Pittsburgh.  Holly & her husband Derek (+ two adorable tween daughters!) have spent the past 13 years serving as Global Partners in China. She has equally enjoyed working with cleft palate babies, traveling to countryside orphanages, and in recent years building relationships with women who have been trafficked.  Although her family is now basing out of Pittsburgh, Holly’s unique blend of personal and cross-cultural experiences enable her to live out James 1:27 with even greater compassion & creativity.  Her home business of hand-made products will continue to support ongoing orphan care initiatives both locally & globally.'
			},
			{
				styles: {
					'background': 'url("/images/ExpoMaureenTicich.png") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Maureen Ticich',
				bio: 'Maureen Ticich earned her Master of Social Work at the University of Maryland at Baltimore. She is the mother of a sibling group of four from Bulgaria and a sibling group of three from Haiti. She is currently employed as the Post Adoption Supervisor at Bethany Christian Services in Wexford. In her free time she enjoys traveling with her family.'
			},
			{
				styles: {
					'background': 'url("/images/ExpoKateWadsworth.jpg") no-repeat center center',
					'background-size':'cover'
				},
				name: 'Kate Wadsworth',
				bio: 'Kate Wadsworth is the Public Relations Manager for Light of Life Rescue Mission, an organization that provides food, shelter and hope for Pittsburgh’s poor and homeless.  She first became interested in social justice and the responsibility of privilege while receiving her M.S. in Counseling at the University of Vermont.  Since that time, Kate has spent time volunteering with refugees, urban youth, and doing international mission work.  Being in diverse community excites her and she loves having conversations about how we can use what God gives us for the good of the Kingdom.'
			},
			{
				styles: {
					'background': 'url("/images/ExpoJohnPorcari.jpg") no-repeat center center',
					'background-size':'cover'
				},
				name: 'John Porcari',
				bio: 'John Porcari is the Serving Leader and Greater Goal Coaching Practice Lead at Third River Partners, a Leader Development and Strategy Execution Consultancy who works with Senior Executive Teams to achieve dramatic results to their business. John’s experience with foster care began when he was a young child and experienced several placements in various foster homes.  He and his wife Mary Beth began their journey into foster care in 2009 as they welcomed their first placement- 3 day old Alanna- into their home. Through God’s great grace, that journey also led to their Adopting Alanna one year later. John and his wife Mary Beth coordinate the leadership team for The Bible Chapel’s Foster and Adoption Ministry. The focus of this ministry includes awareness, equipping/certification, mentoring of foster and adoptive families, outreach to women with children in care, and adoption scholarship assistance.'
			}
		];
	}


	function getExpoSchedule() {
		return [
			{
				time: '8:00 AM',
				title: 'Registration'
			},
			{
				time: '8:30 AM',
				title: 'General Session',
				details: 'Personal Testimony: Amy Brooks\nKeynote Speaker:  Mandy Litzke “All For His Glory”'
			},
			{
				time: '9:45 AM',
				title: 'Break'
			},
			{
				time: '10:15 AM',
				breakouts : [ 
					{
						track : 'Justice',
						title: 'Domestic Minor Sex Trafficking – What is Really Happening and What You Can Do',
						leader: 'Phil Gazley, Kern Coalition, YWAM',
						content: 'Come find out what really is happening with sex trafficking of minors right here in our own country. You will learn how to identify victims and partner with law enforcement, become aware of short and long term services, and be equipped with tools for engagement and prevention. This training was originally developed for high-school counselors.'
					},
					{
						track: 'Student',
						title: 'Justice 101 & Trafficking Simulation',
						leader: 'Erica Chevalier, North Way Justice Team',
						content: 'Social Justice, Trafficking and Modern Day Slavery are buzzwords we often hear in the news and in our churches, but what is this really all about? Come learn the basics of what it means to seek justice and advocate for the oppressed. You will also participate in a simulation designed to give you a realistic glimpse into the world of victims.'
					},
					{
						track: 'Adoption',
						title: 'First Steps in Your Adoption Journey (1 ½ hour session)',
						leader: 'Erin Brothers, Kristin Himmler and Meg McKivigan',
						content: 'If you’ve felt the call to adopt, you’re probably faced with a hundred questions. Join three adoptive moms who have intimate experience of the road to adoption to better understand the first steps necessary to begin your journey. Sharing portions of their own adoption experiences in international, domestic infant and foster-to-adopt, they will help you understand the adoption process, each member of the adoption triad, adoption terminology, common adoption misconceptions and myths, and share some ideas to help you finance your adoption.'
					},
					{
						track: 'Adoption',
						title: 'Foster Care Q & A Parent Panel (1 ½ hour session)',
						leader: 'Kate Grant, Washington County Children & Youth Services',
						content: 'What do I need to do to become a foster parent?  How long does a child typically stay in care? Can I request the age, race or sex of a child placed in my home?  Isn’t it difficult when the child leaves?  Is foster care a good path to adoption?  Join a foster-care social worker and a panel of current foster parents as they candidly answer your questions and share their foster-care experiences.'
					},
					{
						track: 'Adoption',
						title: 'A Warrior Through the Struggle: Parenting & Nurturing As They Heal',
						leader: 'Mandy Litzke, Safe Harbor Orphan Care Ministries ',
						content: 'You’ve gone through the foster or adoption process and now the reality of parenting children from hard places sets in. In this session, an adoptive mom will share how to courageously fight for your kids as they heal. Come learn how to be the warrior God intended you to be for your child, including practical steps for nurturing your child, yourself and your family through the seasons of struggle.'
					},
					{
						track: 'Adoption',
						title: 'For Adoptees Only',
						leader: 'Holly Seipp',
						content: 'Calling all adopted kids (ages tweens - 100!)  This breakout session is designed especially for you! This session will be hosted by an adult adoptee who would love to meet and chat with you about anything adoption related that is on your mind.  Join us for this informal meet and greet.  Can’t wait to meet you!'
					},
					{
						track: 'Ministry',
						title: 'A Biblical Foundation for your Orphan-Related Ministry',
						leader: 'Matthew Geppert, Southeast Asia Prayer Center',
						content: 'A successful ministry for meeting the needs of the orphaned and abandoned around the world must begin with God. Come learn how to effectively use the power of prayer to determine your calling, vision and purpose.  Know who you are trying to reach, and equip yourself for success and multiplying success to others.  If you are planning to adopt or sponsor a child or plan to start a nonprofit or orphan-related ministry, this session will encourage and give you tools for effective ministry.'
					},
					{
						track: 'Ministry',
						title: 'Impacting a Fatherless Generation',
						leader: 'Seth Reichert, Urban Impact',
						content: 'Fatherlessness is a national crisis that must be addressed by the body of Christ.  Today in America, a staggering 41% of all births are out of wedlock and 1 out of 3 children are born into fatherless homes.  The ripple effect of this devastating tragedy can be seen in our homes, our neighborhoods, our cities, our state, our country and even around the world.  This fatherless generation is at a greater risk than ever before to experience homelessness, adjudication, drug abuse, alcoholism, violent crimes, suicide, teen pregnancy, poor education and poverty.  It is time for men and women of God to rise up, stand in the gap and reverse the cycle of fatherlessness through the power and love of Jesus Christ.  Come be inspired as you discover your part in impacting a fatherless generation. '
					}
				]
			},
			{
				time: '11:30 AM',
				breakouts : [ 
					{
						track: 'Justice',
						title: 'Human Trafficking and Migration',
						leader: 'Phil Gazley, Kern Coalition, YWAM',
						content: 'We live in a time where there is more migration in the world than any other time in human history.  Come discover how this reality increases vulnerability for refugees, immigrants, asylum seekers and stateless individuals to human trafficking. We will discuss viable solutions for Pittsburgh, our country and the nations.'
					},
					{
						track: 'Student',
						title: 'Empowered By Your Story',
						leader: 'Shannon Libengood, North Way Missions Director ',
						content: 'Come hear what God is doing through student and young-adult short-term mission teams. Learn how God uses hands-on experiences – both positive and painful – to inform and empower our perspectives, priorities and service.  You will be inspired by stories of God at work in and through the lives of people like you.'
					},
					{
						track: 'Adoption',
						title: 'First Steps in Your Adoption Journey (continued from session 1)',
						leader: 'Erin Brothers, Kristin Himmler and Meg McKivigan',
						content: 'If you’ve felt the call to adopt, you’re probably faced with a hundred questions. Join three adoptive moms who have intimate experience of the road to adoption to better understand the first steps necessary to begin your journey. Sharing portions of their own adoption experiences in international, domestic infant and foster-to-adopt, they will help you understand the adoption process, each member of the adoption triad, adoption terminology, common adoption misconceptions and myths, and share some ideas to help you finance your adoption.'
					},
					{
						track: 'Adoption',
						title: 'Foster Care Q & A Parent Panel (continued from session 1)',
						leader: 'Kate Grant, Washington County Children & Youth Services',
						content: 'What do I need to do to become a foster parent?  How long does a child typically stay in care? Can I request the age, race or sex of a child placed in my home?  Isn’t it difficult when the child leaves?  Is foster care a good path to adoption?  Join a foster-care social worker and a panel of current foster parents as they candidly answer your questions and share their foster-care experiences.'
					},
					{
						track: 'Adoption',
						title: 'Beyond the Five Senses:  Exploring Sensory Processing Disorder',
						leader: 'Meghan Nagle & Kelly Ryan-Schmidt, Three Rivers Adoption Council',
						content: 'Many of the symptoms associated with ADHD/ADD, ODD, RAD, PTSD, etc. can be attributed to sensory processing difficulties.  Sensory processing is how we see the world and take in information through our internal and external senses.  Adverse experiences in early childhood can make taking in information through our bodies much more difficult.  This breakout will allow the opportunity to experience sensory processing and learn new ways to help your child meet their sensory needs in the home and school settings.'
					},
					{
						track: 'Ministry',
						title: 'Simple Ways Your Church Can Help the Orphan',
						leader: 'Orphan Alliance of SWPA',
						content: 'There are over 143 million orphans around the world and less than 1% of these children will be adopted. These children are precious in the sight of God. In fact, they are of infinite value! For that reason, a life-giving plan of action must be enacted that will bring hope to these children. That life-giving action is called Orphan Care. Using the Acronym SIMPLE, The Orphan Alliance of Southwestern PA will assist you in identifying ways you and your church can begin to care for orphans … simply.'
					},
					{
						track: 'Ministry',
						title: 'Weathering the Storm: Encouragement for Those Working in Difficult Ministries',
						leader: 'Joel Repic, Aliquippa Impact',
						content: 'Ministry in difficult contexts can leave you feeling cynical, wounded and discouraged.  How do you sustain and even thrive while serving the vulnerable, abandoned and at-risk?  Learn practical strategies for you and your team to keep your eyes on Christ and serve with His energy. If you feel tired in ministry, this session is for you.'
					}
				]
			},
			{
				time: '12:30 PM',
				title: 'Lunch'
			},
			{
				time: '1:00 PM',
				title: '30 Minutes to Impact',
				details: 'Phil Gazley – “Human Trafficking: What Can I Do?\nMatt Geppert – “Appointed for Such a Time As This”\nJane Abbate – “The Secrets We Keep”\nSeth Reichart – “Impacting a Fatherless Generation”'
			},
			{
				time: '1:30 PM',
				title: 'Break'
			},
			{
				time: '2:00 PM',
				breakouts : [
					{
						track: 'Justice',
						title: 'Effective Anti-Human Trafficking Coalition Development',
						leader: 'Phil Gazley, Kern Coalition, YWAM',
						content: 'This session will help both those wanting to know their place within existing coalitions and those wanting to start and pioneer coalitions within their own communities.  This issue will be looked at through the lens of the “Four P” approach, so that individuals can take on a comprehensive approach to anti-human-trafficking efforts in their communities.'
					},
					{
						track: 'Student',
						title: 'Be Informed & Get Involved',
						leader: 'OrphanCare Ministry Team',
						content: 'This session will provide YOU the opportunity to serve the most vulnerable in our world.  Learn about how Buckner International and the Shoes for Orphan Soul drive impacts children around the world.  Then jump right in and help us pack the shoes that North Way has collected this fall.'
					},
					{
						track: 'Adoption',
						title: 'Adult Adoptee Panel',
						leader: 'John Procari, Adoptive Father',
						content: 'Our panel of adult adoptees ages 16 to 50 share their unique insights into the ways that adoption has impacted their lives, from their feelings about identity to their relationships and more.  This breakout will give perspective and provide adoptive parents insight into what their children may experience, think and feel as adopted individuals. There will be an open question-and-answer time. Come with your questions ready.'
					},
					{
						track: 'Adoption',
						title: 'Understanding Teenage Development and the Impact of Adoption',
						leader: 'Maureen Ticich & Linda Prevost, Bethany Christian Services',
						content: 'Adopted children may not begin to question their role in their family (or become curious about their natural identity) until they begin to enter adolescence. This makes sense because adolescence is typically a time of striving to define oneself, of questioning who one is and what one’s purpose is. As adopted children begin to experience the physical and emotional changes that almost every child experiences during adolescence, however, they may find that they also begin to discover previously unrealized emotional issues related to their adoption. Even if an adopted child has previously appeared to be happy and “well adjusted,” it is not uncommon for adopted adolescents to begin to experience feelings of grief, loss, abandonment, rejection and even guilt or shame. This breakout will help the attendees to better understand how teenagers are processing the fact that they are adopted in new ways and how to assist their children in forming a healthy self-identity.'
					},
					{
						track: 'Ministry',
						title: 'From Grief to Grace',
						leader: 'Jane Abbate, Messy Miracles',
						content: 'A great price is paid by women and men who have chosen to terminate a pregnancy through abortion.  There is often a stressful, soul-searching period extending for years and decades, which often goes unnoticed and unsupported.  This session will provide pastors, ministry leaders, family and friends a better understanding of the nature and impact of unresolved abortion grief and what steps can be taken to help those who have experienced abortion to accept God’s mercy and grace. This is an opportunity to grow in understanding and compassion for those who are struggling and to discover ways to create a more understanding and healing community. Embrace Life Ministries and Women’s Choice Network will provide tangible ways for you to get involved.'
					},
					{
						track: 'Ministry',
						title: 'World Without Orphans - An International Church Movement!',
						leader: 'Ron Goodman, Orphan Helpers',
						content: 'You will gain awareness of a worldwide movement that mobilizes the international church to provide care for children who have been orphaned, abandoned, exploited, physically or sexually abused, neglected, or aging out of institutional care.  Christian leaders are just beginning to fully recognize that churches throughout the world – including the United States – have the resources to provide needed services to these children and their families in their own countries.  You will learn about the important work of the Christian Alliance for Orphans (CAFO) in this initiative and how many mission organizations are getting involved.  We will also discuss how Orphan Helpers, an organization working with children in government orphanages and juvenile detention centers in El Salvador and Honduras, is collaborating within this movement.'
					},
					{
						track: 'Ministry',
						title: 'Exploring Race, Poverty and the Responsibility of Privilege',
						leader: 'Kate Wadsworth, Light of Life',
						content: 'In this session we will discuss the myth of being “colorblind” (the idea that people of all races have the same experience), the mindset of living in poverty, and how living in America shapes the lens in which we view the world and other cultures. This discussion seeks to help us become more aware of privileges we may not know we have and how privilege is a reality that needs to be understood, whether we are mentoring at-risk youth, serving birth moms, adopting and fostering, or heading to the mission field. Learn how growing up in America gives us a perspective that needs to be understood, and how we can do ministry with brothers and sisters of all races, cultures and socio-economic levels.'
					}
				]	
			}
		];
	}

});
