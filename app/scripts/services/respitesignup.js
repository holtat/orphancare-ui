'use strict';

angular.module('orphanCareApp')
.service('Respitesignup', function Respitesignup($http) {

		return {
				submitSignup : function(data) {
						var json = angular.toJson(data);
						$http({
								url: 'http://nworphancare.com/api/RespiteNight',
								method: "POST",
								data: json,
								headers: {'Content-Type': 'application/json'}
						}).success(function (data, status, headers, config) {
								console.log(status);
						}).error(function (data, status, headers, config) {
								console.log(status);
						});
				}
		}
});
