'use strict';

angular.module('orphanCareApp')
.factory('Exporegistration', function Exporegistration($http, toaster) {
	var service = {
		submitSignup : SubmitSignup,
		getSignups : GetSignups
	};

	return service;
	///////////////////
	function GetSignups(complete) {
		$http({
			url: 'http://nworphancare.org/api/ExpoRegistration',
			method: "GET",
		}).success(function (data, status, headers, config) {
			toaster.pop('success', "Success", "Here's the signups");
			complete(data);
		}).error(function (data, status, headers, config) {
			toaster.pop('error', "Whoops", data.Message);
			complete(data);
		});	
	}

	function SubmitSignup(data) {
		var json = angular.toJson(data);
		$http({
			url: 'http://nworphancare.org/api/ExpoRegistration',
			method: "POST",
			data: json,
			headers: {'Content-Type': 'application/json'}
		}).success(function (data, status, headers, config) {
			toaster.pop('success', "Great!", "You're signed up.");
		}).error(function (data, status, headers, config) {
			toaster.pop('error', "Sorry", data + " (" + status + ")");
			console.log(status);
		});
	}

});