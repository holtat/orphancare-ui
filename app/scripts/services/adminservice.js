'use strict';

angular.module('orphanCareApp')
.service('Adminservice', function Adminservice($http, toaster, $rootScope) {
	return {
		login : function(data, complete) {
			var json = angular.toJson(data);
			$http({
				url: $rootScope.baseAddress + 'Account/Login',
				method: "POST",
				data: json,
				headers: {'Content-Type': 'application/json'}
			}).success(function (data, status, headers, config) {
				complete(true);
			}).error(function (data, status, headers, config) {
				toaster.pop('error', "Oops", data.Message);
				complete(false);
			});
		}
	};
});
