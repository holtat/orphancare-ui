'use strict';
angular
.module('orphanCareApp', [
	'ngCookies',
	'ngResource',
	'ngSanitize',
	'ngAnimate',
	'ngRoute',
	'ngGrid',
	'angular-loading-bar',
	'toaster',
	'datatables',
	'checklist-model',
	'ui.bootstrap'
])
.config(function ($routeProvider) {
	$routeProvider
	.when('/', {
		templateUrl: 'views/main.html',
		controller: 'MainCtrl'
	})
	.when('/Main', {
		templateUrl: 'views/main.html',
		controller: 'MainCtrl'
	})
	.when('/Adoption', {
		templateUrl: 'views/adoption.html',
		controller: 'AdoptionCtrl'
	})
	.when('/Newsletters', {
		templateUrl: 'views/newsletters.html',
		controller: 'NewslettersCtrl'
	})
	.when('/FosterCare', {
		templateUrl: 'views/fostercare.html',
		controller: 'FostercareCtrl'
	})
	.when('/404', {
		templateUrl: '404.html',
	})
	.when('/test', {
		templateUrl: 'views/test.html',
		controller: 'TestCtrl'
	})
	.when('/admin', {
		templateUrl: 'views/admin.html',
		controller: 'AdminCtrl'
	})
	.when('/admintable', {
		templateUrl: 'views/admintable.html',
		controller: 'AdmintableCtrl'
	})
	.when('/ministrypartners', {
		templateUrl: 'views/ministrypartners.html',
		controller: 'MinistrypartnersCtrl'
	})
	.when('/expo', {
		templateUrl: 'views/expo.html',
		controller: 'ExpoCtrl'
	})
.when('/exposchedule', {
  templateUrl: 'views/exposchedule.html',
  controller: 'ExposcheduleCtrl'
})
.when('/expospeakers', {
  templateUrl: 'views/expospeakers.html',
  controller: 'ExpospeakersCtrl'
})
.when('/expofaq', {
  templateUrl: 'views/expofaq.html',
  controller: 'ExpofaqCtrl'
})
	.otherwise({
		redirectTo: '/404'
	});
})
.run(function($rootScope, $location, $anchorScroll, $routeParams) {
	$rootScope.baseAddress = "http://nworphancare.org/api/";

	$rootScope.$on('$routeChangeSuccess', function(newRoute, oldRoute) {
		$location.hash($routeParams.scrollTo);
		$anchorScroll();  

		$rootScope.showNav = true;

		if ($location.path() == "/404") {
			$rootScope.showNav = false;
		}
	});
});

