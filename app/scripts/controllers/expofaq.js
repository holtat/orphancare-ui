'use strict';

angular.module('orphanCareApp')
.controller('ExpofaqCtrl', function ($scope, Exposcheduleservice) {
	$scope.faqs = Exposcheduleservice.getFaqs();
});
