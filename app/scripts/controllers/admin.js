'use strict';

angular.module('orphanCareApp')
  .controller('AdminCtrl' , function ($scope, Adminservice, $location) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
	
		$scope.loginModel = {};
	
		$scope.login = function() {
			var success = Adminservice.login($scope.loginModel, function(success) {
				if (success) {
					$location.url("/admintable");
				} else {
					$location.url("/admin");
				}
			});
			
		};
	
  });
