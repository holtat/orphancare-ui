'use strict';

angular.module('orphanCareApp')
  .controller('TestCtrl', function ($scope, Expo2014signup) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
		
		$scope.submitSignup = function ( ) {
			Expo2014signup.submitSignup($scope.signup, function() {			});
		};
		
		$scope.signup = {};
  });
