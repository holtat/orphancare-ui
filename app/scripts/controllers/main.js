'use strict';

angular.module('orphanCareApp')
  .controller('MainCtrl', function ($scope, UpcomingEventservice, Respitenightservice) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
		
		$scope.upcomingEvents = UpcomingEventservice.getUpcomingEvents();
		
		$scope.longDateString = function(date) {			
			return date.toDateString();
		};
		
		$scope.signup = {};
		
		$scope.submitSignup = function() {
			Respitenightservice.submitSignup($scope.signup, function() {
				
			});
		}
  });
