'use strict';

angular.module('orphanCareApp')
.controller('ExpoCtrl', function ($scope, Exporegistration, Fixie) {

	$scope.aboutMe = new GetAboutMe();
	$scope.signup = {};
	$scope.submitSignup = SubmitSignup;
	$scope.breakoutOptions = new GetBreakoutOptions();
	$scope.fixIE = Fixie.fixIE;
	
	////////////////////
	function GetAboutMe() {
		return [
			"Looking for ways to help orphans",
			"Looking to start an Orphan ministry",
			"Considering adoption of foster care",
			"Adoptive/foster parent",
			"Adoptive/foster care professional",
			"Church ministry leader",
			"Interested in missions to orphans",
			"Interested in trafficking/slavery issues"
		];
	}

	function GetBreakoutOptions() {
		return {
			session1 : [
				"Domestic Minor Sex Trafficking",
				"Justice 101 & Trafficking Simulation",
				"First Steps in Your Adoption Journey (1.5 hrs)",
				"Foster Care Q & A Parent Panel (1.5 hrs)",
				"A Warrior Through the Struggle",
				"For Adoptees Only",
				"A Biblical Foundation for your Orphan-Related Ministry",
				"Impacting a Fatherless Generation"
			],
			session2 : [
				"Human Trafficking and Migration",
				"Empowered by Your Story",
				"First Steps in your Adoption Journey (continued)",
				"Foster Care Q & A Parent Panel (continued)",
				"Beyond the Five Senses: Sensory Processing",
				"SIMPLE Ways Your Church Can Help the Orphan",
				"Weathering the Storm: Encouragement"
			],
			session3 : [
			"Effective Anti-Human Trafficking Coalition Development",
				"Be Informed & Get Involved",
				"Adult Adoptee Panel",
				"Understanding Teen Development and the Impact of Adoption",
				"From Grief to Grace",
				"Exploring Race, Poverty and the Responsibility of Privilege",
				"World Without Orphans:  A International Church Movement"
			]
		};
	}

	function SubmitSignup() {
		if ($scope.signup.AboutMe !== undefined)
			$scope.signup.AboutMe = $scope.signup.AboutMe.join().replace(/,/g, ", ");
		Exporegistration.submitSignup($scope.signup);
	}
});
