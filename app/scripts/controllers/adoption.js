'use strict';

angular.module('orphanCareApp')
.controller('AdoptionCtrl', function ($scope) {
		$scope.awesomeThings = [
				'HTML5 Boilerplate',
				'AngularJS',
				'Karma'
		];  

		//Save Form
		$('#saveSignup').click(function (e) {
				e.preventDefault();

				var form = {};

				$('input').each(function (i, elem) {
						if (elem.value && elem.value != "Other") {
								form[elem.name] = elem.value;
						}
				});

				$.ajax({
						url: 'http://nworphancare.org/Services.svc/ExploringAdoptionSignups',
						type: 'post',
						data: JSON.stringify(form),
						headers: {
								"Content-Type": "application/json;odata=verbose"
						},
						success: function (data) {
								console.info(data);
								$('#signup').modal('hide');
						},
						error: function(xhr, status, error) {
								console.info(error);
								$('#saveSignup').each(function () {
										$(this).parent().append('<p style="color: red; margin-top:7px;">' + "An error occured while signing up" + '</p>');
								});
						}
				});
		});

		//Collection View Data
		
		$scope.agencies = [
				{
						style: {
								background: "url('/images/adoption-agencies/adoption-connection-pa.png')"
						},
						title: "Adoption Connection PA",
						link: "http://www.adoptionconnectionpa.org"
				},
				{
						style: {
								background: "url('/images/adoption-agencies/adoptions-from-the-heart.png')"
						},
						title: "Adoptions From The Heart",
						link: "http://www.adoptionsfromtheheart.org"
				},
				{
						style: {
								background: "url('/images/adoption-agencies/america-world-adoption.png')"
						},
						title: "America World Adoption",
						link: "http://www.awaa.org"
				},
				{
						style: {
								background: "url('/images/adoption-agencies/bethany-christian-services.png')"
						},
						title: "Bethany Christian Services",
						link: "http://www.bethany.org/pittsburgh"
				},
				{
						style: {
								background: "url('/images/adoption-agencies/catholic-charities.png')"
						},
						title: "Catholic Charities",
						link: "http://www.ccharitiesgreensburg.org/adoption/Pages/default.aspx"
				},
				{
						style: {
								background: "url('/images/adoption-agencies/childrens-institute.png')"
						},
						title: "The Children's Institute",
						link: "http://www.amazingkids.org"
				},
				{
						style: {
								background: "url('/images/adoption-agencies/holt-international.png')"
						},
						title: "Holt International",
						link: "http://www.holtinternational.org"
				},
				{
						style: {
								background: "url('/images/adoption-agencies/childrens-home.png')"
						},
						title: "The Children's Home",
						link: "http://www.childrenshomepgh.org/adoption"
				},
				{
						style: {
								background: "url('/images/adoption-agencies/every-child-inc.png')"
						},
						title: "Every Child Inc.",
						link: "http://www.everychildinc.org"
				},
				{
						style: {
								background: "url('/images/adoption-agencies/three-rivers-adoption-council.png')"
						},
						title: "Three Rivers Adoption Council",
						link: "http://www.3riversadopt.org"
				},
				{
						style: {
								background: "url('/images/adoption-agencies/try-again-homes.png')"
						},
						title: "Try Again Homes",
						link: "http://www.try-againhomes.org"
				},
				{
						style: {
								background: "url('/images/adoption-agencies/wesley-spectrum-services.png')"
						},
						title: "Wesley Spectrum Services",
						link: "http://www.wesleyspectrum.org"
				}
		];
});
