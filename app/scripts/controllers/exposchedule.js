'use strict';

angular.module('orphanCareApp')
.controller('ExposcheduleCtrl', function ($scope, Exposcheduleservice) {
	$scope.oneAtATime = false;

	$scope.sessions = Exposcheduleservice.getExpoSchedule();

	$scope.showTitle = function(session) {
		return session.title != undefined;
	};
	
	$scope.showDetails = function(session) {
		return session.details != undefined;
	};
	
	$scope.trackColorForBreakout = function(track) {
		if (track == 'Justice') {
			return { 'background' : '#FF6161' };
		} else if (track == 'Student') {
			return { 'background' : '#B8E986' };
		} else if (track == 'Adoption') {
			return { 'background' : '#641264' };
		} else if (track == 'Ministry') {
			return { 'background' : '#68ADFD' }; 
		} else {
			return { 'display' : 'none' };
		}
	};
	
	$scope.status = {
		isFirstOpen: false,
		isFirstDisabled: false
	};
});
