'use strict';

angular.module('orphanCareApp')
  .controller('NewslettersCtrl', function ($scope, $routeParams, Newsletterservice) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
		
		$scope.newsletters = Newsletterservice.getNewsletters();
		
		if ($routeParams.newsletter) {
			var i = 0, found = false;
			for (i = 0; i < $scope.newsletters.length; i++) {
				if ($routeParams.newsletter == $scope.newsletters[i].id) {
					$scope.activeNewsletter = $scope.newsletters[i];
					found = true;
				}
			}
			
			if (!found){
				$scope.activeNewsletter = $scope.newsletters[0];
			}
		} else {
			$scope.activeNewsletter = $scope.newsletters[0];
		}
			
		$scope.pickedNewsletter = function(index) {
			$scope.activeNewsletter = $scope.newsletters[index];
		}
		
  });
