'use strict';

angular.module('orphanCareApp')
.controller('ExpospeakersCtrl', function ($scope, Exposcheduleservice) {
	
	$scope.speakers = Exposcheduleservice.getExpoSpeakers();
	
});
