'use strict';

angular.module('orphanCareApp')
.controller('AdmintableCtrl', function ($scope, Exporegistration, DTOptionsBuilder, DTColumnDefBuilder ) {
	$scope.awesomeThings = [
		'HTML5 Boilerplate',
		'AngularJS',
		'Karma'
	];

	$scope.signups = [];

	$scope.dtOptions = DTOptionsBuilder.newOptions().withOption('scrollX',true).withBootstrap()

	var s = '50px';
	var m = '100px';
	var l = '150px';
	var xl = '200px';

	$scope.dtColumnDefs = [
		DTColumnDefBuilder.newColumnDef(1).withOption('sWidth', m),
		DTColumnDefBuilder.newColumnDef(2).withOption('sWidth', m),
		DTColumnDefBuilder.newColumnDef(3).withOption('sWidth', l),
		DTColumnDefBuilder.newColumnDef(4).withOption('sWidth', l),
		DTColumnDefBuilder.newColumnDef(5).withOption('sWidth', xl),
		DTColumnDefBuilder.newColumnDef(6).withOption('sWidth', xl),
		DTColumnDefBuilder.newColumnDef(7).withOption('sWidth', l),
		DTColumnDefBuilder.newColumnDef(8).withOption('sWidth', l),
		DTColumnDefBuilder.newColumnDef(11).withOption('sWidth', '300px'),
		DTColumnDefBuilder.newColumnDef(12).withOption('sWidth', xl),
		DTColumnDefBuilder.newColumnDef(13).withOption('sWidth', xl),
		DTColumnDefBuilder.newColumnDef(14).withOption('sWidth', xl),
		DTColumnDefBuilder.newColumnDef(16).withOption('sWidth', xl)
	];

	Exporegistration.getSignups(function(data) {
		$scope.signups = data;
	});

});
