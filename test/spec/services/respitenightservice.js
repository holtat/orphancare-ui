'use strict';

describe('Service: Respitenightservice', function () {

  // load the service's module
  beforeEach(module('orphanCareApp'));

  // instantiate service
  var Respitenightservice;
  beforeEach(inject(function (_Respitenightservice_) {
    Respitenightservice = _Respitenightservice_;
  }));

  it('should do something', function () {
    expect(!!Respitenightservice).toBe(true);
  });

});
