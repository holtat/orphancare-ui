'use strict';

describe('Service: Exporegistration', function () {

  // load the service's module
  beforeEach(module('orphanCareApp'));

  // instantiate service
  var Exporegistration;
  beforeEach(inject(function (_Exporegistration_) {
    Exporegistration = _Exporegistration_;
  }));

  it('should do something', function () {
    expect(!!Exporegistration).toBe(true);
  });

});
