'use strict';

describe('Service: Expo2014signup', function () {

  // load the service's module
  beforeEach(module('orphanCareApp'));

  // instantiate service
  var Expo2014signup;
  beforeEach(inject(function (_Expo2014signup_) {
    Expo2014signup = _Expo2014signup_;
  }));

  it('should do something', function () {
    expect(!!Expo2014signup).toBe(true);
  });

});
