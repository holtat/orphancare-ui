'use strict';

describe('Service: Respitesignup', function () {

  // load the service's module
  beforeEach(module('orphanCareApp'));

  // instantiate service
  var Respitesignup;
  beforeEach(inject(function (_Respitesignup_) {
    Respitesignup = _Respitesignup_;
  }));

  it('should do something', function () {
    expect(!!Respitesignup).toBe(true);
  });

});
