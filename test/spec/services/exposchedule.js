'use strict';

describe('Service: Exposchedule', function () {

  // load the service's module
  beforeEach(module('orphanCareApp'));

  // instantiate service
  var Exposchedule;
  beforeEach(inject(function (_Exposchedule_) {
    Exposchedule = _Exposchedule_;
  }));

  it('should do something', function () {
    expect(!!Exposchedule).toBe(true);
  });

});
