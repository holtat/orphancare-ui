'use strict';

describe('Service: Fixie', function () {

  // load the service's module
  beforeEach(module('orphanCareApp'));

  // instantiate service
  var Fixie;
  beforeEach(inject(function (_Fixie_) {
    Fixie = _Fixie_;
  }));

  it('should do something', function () {
    expect(!!Fixie).toBe(true);
  });

});
