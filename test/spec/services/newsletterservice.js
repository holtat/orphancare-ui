'use strict';

describe('Service: Newsletterservice', function () {

  // load the service's module
  beforeEach(module('orphanCareApp'));

  // instantiate service
  var Newsletterservice;
  beforeEach(inject(function (_Newsletterservice_) {
    Newsletterservice = _Newsletterservice_;
  }));

  it('should do something', function () {
    expect(!!Newsletterservice).toBe(true);
  });

});
