'use strict';

describe('Controller: ExpofaqCtrl', function () {

  // load the controller's module
  beforeEach(module('orphanCareApp'));

  var ExpofaqCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ExpofaqCtrl = $controller('ExpofaqCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
