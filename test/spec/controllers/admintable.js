'use strict';

describe('Controller: AdmintableCtrl', function () {

  // load the controller's module
  beforeEach(module('orphanCareApp'));

  var AdmintableCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdmintableCtrl = $controller('AdmintableCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
