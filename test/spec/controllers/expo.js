'use strict';

describe('Controller: ExpoCtrl', function () {

  // load the controller's module
  beforeEach(module('orphanCareApp'));

  var ExpoCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ExpoCtrl = $controller('ExpoCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
