'use strict';

describe('Controller: FostercareCtrl', function () {

  // load the controller's module
  beforeEach(module('orphanCareApp'));

  var FostercareCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FostercareCtrl = $controller('FostercareCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
