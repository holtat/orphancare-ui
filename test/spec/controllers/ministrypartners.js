'use strict';

describe('Controller: MinistrypartnersCtrl', function () {

  // load the controller's module
  beforeEach(module('orphanCareApp'));

  var MinistrypartnersCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MinistrypartnersCtrl = $controller('MinistrypartnersCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
