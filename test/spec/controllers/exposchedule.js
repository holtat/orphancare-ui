'use strict';

describe('Controller: ExposcheduleCtrl', function () {

  // load the controller's module
  beforeEach(module('orphanCareApp'));

  var ExposcheduleCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ExposcheduleCtrl = $controller('ExposcheduleCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
